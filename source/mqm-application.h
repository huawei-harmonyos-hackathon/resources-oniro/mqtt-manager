/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 * @file mqm-application.h
 */

#pragma once

#include "mqm-defaults.h"
#include "mqm-options.h"
#include "mqm-types.h"
#ifdef WITH_SYSTEMD
#include "mqm-sdnotify.h"
#endif
#include "mqm-adapter.h"
#include "mqm-dbusown.h"
#include "mqm-executor.h"

#include <glib.h>
#include <stdlib.h>

G_BEGIN_DECLS

/**
 * @brief MqmApplication application object referencing main objects
 */
typedef struct _MqmApplication {
    MqmOptions *options;
#ifdef WITH_SYSTEMD
    MqmSDNotify *sdnotify;
#endif
    MqmDBusOwn *dbusown;
    MqmExecutor *executor;
    MqmAdapter *adapter;
    GMainLoop *mainloop;
    grefcount rc;
} MqmApplication;

/**
 * @brief Create a new MqmApplication object
 * @param config Full path to the configuration file
 * @param error An error object must be provided. If an error occurs during
 * initialization the error is reported and application should not use the
 * returned object. If the error is set the object is invalid and needs to be
 * released.
 */
MqmApplication *mqm_application_new(const gchar *config, GError **error);

/**
 * @brief Aquire MqmApplication object
 * @param app The object to aquire
 * @return The aquiered MqmApplication object
 */
MqmApplication *mqm_application_ref(MqmApplication *app);

/**
 * @brief Release a MqmApplication object
 * @param app The mqm application object to release
 */
void mqm_application_unref(MqmApplication *app);

/**
 * @brief Execute mqm application
 * @param app The mqm application object
 * @return If run was succesful MQM_STATUS_OK is returned
 */
MqmStatus mqm_application_execute(MqmApplication *app);

/**
 * @brief Get main event loop reference
 * @param app The mqm application object
 * @return A pointer to the main event loop
 */
GMainLoop *mqm_application_get_mainloop(MqmApplication *app);

G_DEFINE_AUTOPTR_CLEANUP_FUNC(MqmApplication, mqm_application_unref);

G_END_DECLS
